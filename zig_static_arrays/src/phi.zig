const std = @import("std");

pub fn calculatePhi(dimension: usize) f64 {
    var phi: f64 = 2.0;
    const denominator = @intToFloat(f64, dimension + 1);
    const power = 1.0 / denominator;
    var i: usize = 0;
    while (i < 25) : (i += 1) {
        phi += 1;
        phi = std.math.pow(f64, phi, power);
    }
    return phi;
}

pub fn createAlphas(comptime dimension: usize) [dimension]f64 {
    var alphas = [_]f64{0.0} ** dimension;
    const phi = calculatePhi(dimension);
    const inv_g = 1.0 / phi;
    var i: usize = 1;
    while (i <= dimension) : (i += 1) {
        alphas[i - 1] = fract(std.math.pow(f64, inv_g, @intToFloat(f64, i)));
    }
    return alphas;
}

pub fn fract(value: f64) f64 {
    return value - @trunc(value);
}
