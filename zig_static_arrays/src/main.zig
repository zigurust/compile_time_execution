const std = @import("std");
const phi = @import("phi.zig");
const calculatePhi = phi.calculatePhi;
const createAlphas = phi.createAlphas;

const phi1 = calculatePhi(1);
const phi2 = calculatePhi(2);
const phi3 = calculatePhi(3);

const alphas = createAlphas(2);
comptime {
    @compileLog("alphas[0]=", alphas[0]);
}

pub fn main() !void {
    std.debug.print("phi(1)={}. phi(2)={}. phi(3)={}.\n", .{ phi1, phi2, phi3 });

    for (alphas) |alpha, i| {
        std.debug.print("alphas[{}]={}\n", .{ i, alpha });
    }
}
