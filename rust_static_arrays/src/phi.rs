pub const fn calculate_phi(dimension: usize) -> f64 {
    let mut phi: f64 = 2.0;
    let denominator: f64 = (dimension as u32 + 1) as f64;
    let power: f64 = 1.0 / denominator;
    let mut i = 0;
    while i < 25 {
        i += 1;
        phi += 1.0;
        phi *= power; // phi.powf(power);
    }
    phi
}

pub const fn create_alphas<const N: usize>() -> [f64; N] {
    let mut alphas = [0.0; N];
    let phi = calculate_phi(N);
    let inv_g = 1.0 / phi;
    let mut i = 1;
    while i <= N {
        alphas[i - 1] = phi;
        i += 1;
    }
    alphas
}
