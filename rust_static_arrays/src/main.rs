#![feature(const_fn_floating_point_arithmetic)]

use phi::{calculate_phi, create_alphas};

mod phi;

const PHI: f64 = calculate_phi(1);

const ALPHAS: [f64; 6] = create_alphas();

fn main() {
    println!("Phi = {PHI}");
    let inv_g = 1.0 / PHI;
    println!("inv_g = {inv_g}");
    let v = inv_g * inv_g;
    println!("{v}");
    for alpha in ALPHAS {
        println!("{}", alpha);
    }
}
